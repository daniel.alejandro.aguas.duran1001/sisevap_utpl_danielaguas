package SisEvAp_UTPL_DanielAguas;
/**
 *
 * @author DAAD
 */
public class Estudiante {
    
    private String est,ACDB1,APEB1,AAB01,AAEPRB1,AAB1,BIM1,ACDB2,APEB2,AAB02,AAEPRB2,AAB2,BIM2,TOTALFIN,ACU65,RECU,FINAL, ESTADO;
    
	public Estudiante() {	
	}	
	public String getEstudiante() { return est; }
	public String getACDB1() { return ACDB1; }
	public String getAPEB1() { return APEB1; }
	public String getAAB01() { return AAB01; }
	public String getAAEPRB1() { return AAEPRB1; }
	public String getAAB1() { return AAB1; }
	public String getBIM1() { return BIM1; }
	public String getACDB2() { return ACDB2; }
	public String getAPEB2() { return APEB2; }
        public String getAAB02() { return AAB02; }
        public String getAAEPRB2() { return AAEPRB2; }
        public String getAAB2() { return AAB2; }
        public String getBIM2() { return BIM2; }
        public String getTOTALFIN() { return TOTALFIN; }
        public String getACU65() { return ACU65; }
        public String getRECU() { return RECU; }
        public String getFINAL() { return FINAL; }
	public String getEstado() { return ESTADO; }
        
	public void setEstudiante(String E) { this.est = E; }
	public void setACDB1(String dato) { this.ACDB1 = dato;}
	public void setAPEB1(String dato) { this.APEB1 = dato;}
	public void setAAB01(String dato) { this.AAB01 = dato;}
	public void setAAEPRB1(String dato) { this.AAEPRB1 = dato;}
	public void setAAB1(String dato) { this.AAB1 = dato;}
	public void setBIM1(String dato) { this.BIM1 = dato;}
	public void setAACDB2(String dato) { this.ACDB2 = dato;}
	public void setAAPEB2(String dato) { this.APEB2 = dato;}
        public void setAAB02(String dato) { this.AAB02 = dato;}
        public void setAAEPRB2(String dato) { this.AAEPRB2 = dato;}
        public void setAAB2(String dato) { this.AAB2 = dato;}
        public void setBIM2(String dato) { this.BIM2 = dato;}
        public void setTOTALFIN(String dato) { this.TOTALFIN = dato;}
        public void setACU65(String dato) { this.ACU65 = dato;}
        public void setRECU(String dato) { this.RECU = dato;}
        public void setFINAL(String dato) { this.FINAL = dato;}
	public void setEstado(String dato) { this.ESTADO = dato;}
        
}
