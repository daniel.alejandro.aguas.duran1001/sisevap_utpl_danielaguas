/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SisEvAp_UTPL_DanielAguas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author DAAD
 */
public class OperacionArchivo {
    
    	
	//crea el archivo en disco, retorna la lista de estudiantes
	public static ArrayList leerArchivo() {
		// crea el flujo para leer desde el archivo
		File file = new File("Archivos/daaguas3_ProgAlg_O20F21_(APEB2-20)_FilesInput.csv");
		ArrayList listaEstudiantes= new ArrayList<>();	
		Scanner scanner;
		try {
			//se pasa el flujo al objeto scanner
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				// el objeto scanner lee linea a linea desde el archivo
				String linea = scanner.nextLine()+" ";
				Scanner delimitar = new Scanner(linea);
				//se usa una expresión regular
				//que valida que antes o despues de una coma (,) exista cualquier cosa
				//parte la cadena recibida cada vez que encuentre una coma				
				delimitar.useDelimiter(",");
				Estudiante e= new Estudiante();
				e.setEstudiante(delimitar.next());
				e.setACDB1(delimitar.next());
				e.setAPEB1(delimitar.next());
				e.setAAB01(delimitar.next());
				e.setAAEPRB1(delimitar.next());
				e.setAAB1(delimitar.next());
				e.setBIM1(delimitar.next());
				e.setAACDB2(delimitar.next());
				e.setAAPEB2(delimitar.next());
				e.setAAB02(delimitar.next());
				e.setAAEPRB2(delimitar.next());
				e.setAAB2(delimitar.next());
				e.setBIM2(delimitar.next());
				e.setTOTALFIN(delimitar.next());
				e.setACU65(delimitar.next());
				e.setRECU(delimitar.next());
				e.setFINAL(delimitar.next());
				e.setEstado(delimitar.next());
				listaEstudiantes.add(e);
			}
			//se cierra el ojeto scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return listaEstudiantes;
	}
	
        /*public static Double validarValor(String dato){
            Double dat;
            if (dato.equals(""))
                return 0.0;
            else
                return Double.parseDouble(dato);
        }*/
	//añadir más estudiantes al archivo
	public static void aniadirArchivo(ArrayList lista) {
		FileWriter flwriter = null;
		try {//además de la ruta del archivo recibe un parámetro de tipo boolean, que le indican que se va añadir más registros 
			flwriter = new FileWriter("Archivos/daaguas3_ProgAlg_O20F21_(APEB2-20)_FilesOutput.csv", true);
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			for (Estudiante E :(ArrayList<Estudiante>) lista) {
				//escribe los datos en el archivo
				bfwriter.write(E.getEstudiante()+","+E.getACDB1()+","+E.getAPEB1()+","+E.getAAB01()+","+E.getAAEPRB1()+","+E.getAAB1()+","+E.getBIM1()
                                                                +","+E.getACDB2()+","+E.getAPEB2()+","+E.getAAB02()+","+E.getAAEPRB2()+","+E.getAAB2()+","+E.getBIM2()
                                                                +","+E.getTOTALFIN()+","+E.getACU65()+","+E.getRECU()+","+E.getFINAL()+","+E.getEstado() + "\n");
			}
			bfwriter.close();
			System.out.println("Archivo creado satisfactoriamente..");
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}	
    
}
