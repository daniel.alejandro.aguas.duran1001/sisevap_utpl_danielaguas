/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SisEvAp_UTPL_DanielAguas;

import java.util.ArrayList;

/**
 *
 * @author DAAD
 */
public class TestArchivo {

    private static Double validarValor(String dato) {
        Double dat;
            if (dato.equals(""))
                return 0.0;
            else
                return Double.parseDouble(dato);
    }

    /**
     * @param args the command line arguments
     */
    
        
 
	public static void main(String[] args) {
            int cont=0;
            Double V1, V2, V3, V4, Op;
            int ExPre1=0, ExPre2=0, ExRec=0;
            Double PorEx1, PorEx2, PorRec, cant;
            // estructura Array List para guardar los objetos estudiantes
            //ArrayList listaEstudiantes = new ArrayList<>();

            // lista para recibir los objetos estudiantes desde el archivo
            ArrayList listaLeida = new ArrayList<>();

            // asignar a la lista los objetos
            listaLeida = OperacionArchivo.leerArchivo();
            for (Estudiante E : (ArrayList<Estudiante>) listaLeida) {
                if (cont>0){
                    //AAB1
                    V1 = validarValor(E.getAAB01());
                    V2 = validarValor(E.getAAEPRB1());
                    Op = V1+V2;
                    E.setAAB1(""+Op);
                    //BIM1
                    V1 = validarValor(E.getACDB1());
                    V2 = validarValor(E.getAPEB1());
                    V3 = validarValor(E.getAAB1());
                    Op = V1+V2+V3;
                    E.setBIM1(""+Op);
                    //AAB2
                    V1 = validarValor(E.getAAB02());
                    V2 = validarValor(E.getAAEPRB2());
                    Op = V1+V2;
                    E.setAAB2(""+Op);
                    //BIM2
                    V1 = validarValor(E.getACDB2());
                    V2 = validarValor(E.getAPEB2());
                    V3 = validarValor(E.getAAB2());
                    Op = V1+V2+V3;
                    E.setBIM2(""+Op);
                    //TOTALFIN
                    V1 = validarValor(E.getBIM1());
                    V2 = validarValor(E.getBIM2());
                    Op = (double)Math.round(((V1+V2)/2)*10d)/10;
                    E.setTOTALFIN(""+Op);
                    //ACU65
                    V1 = validarValor(E.getACDB1());
                    V2 = validarValor(E.getAPEB1());
                    V3 = validarValor(E.getACDB2());
                    V4 = validarValor(E.getAPEB2());
                    Op = (double)Math.round(((V1+V2+V3+V4)/2)*100d)/100;
                    E.setACU65(""+Op);
                    //FINAL
                    if (E.getRECU().equals(""))
                        E.setFINAL(E.getTOTALFIN());
                    else{
                        V1 = validarValor(E.getACU65());
                        V2 = validarValor(E.getRECU());
                        Op = (double)Math.round((V1+V2)*10d)/10;
                        E.setFINAL(""+Op);
                    }
                    //ESTADO
                    V1 = validarValor(E.getFINAL());
                    if (V1>=7)
                        E.setEstado("Acreditado");
                    else
                        E.setEstado("No Acreditado");
                    
                    if (validarValor(E.getAAEPRB1())==0)
                        ExPre1++;
                    if (validarValor(E.getAAEPRB2())==0)
                        ExPre2++;
                    if (validarValor(E.getRECU())==0)
                        ExRec++;
                    System.out.println(E.getEstudiante() + " " + E.getACDB1() + " " + E.getAPEB1() + " " 
                            + E.getAAB01() + " " + E.getAAEPRB1());
                }
                //listaNueva.add(E);
                cont++;
            }
            
            cant = Double.parseDouble(""+(cont-1));
            PorEx1 = (double)Math.round((ExPre1/cant)*10000d)/100;
            PorEx2 = (double)Math.round((ExPre2/cant)*10000d)/100;
            PorRec = (double)Math.round((ExRec/cant)*10000d)/100;

            System.out.println("Porcentaje de Inasistencia en Examen 1: "+PorEx1);
            System.out.println("Porcentaje de Inasistencia en Examen 2: "+PorEx2);
            System.out.println("Porcentaje de Inasistencia en Examen Recuperación: "+PorRec);
            OperacionArchivo.aniadirArchivo(listaLeida);
             
		
	}
}

